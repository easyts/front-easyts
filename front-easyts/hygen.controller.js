const { exec } = require("child_process");
const express = require("express");
const router = express.Router();
const fs = require("fs");

const { resolve } = require("path");
const projectPath = resolve(".");

router.post("/", function(req, res) {
  const json = req.body;
  try {
    exec(
      `HYGEN_OVERWRITE=1 hygen model new --params "${JSON.stringify(
        json
      ).replace(/"/g, '\\"')}"`,
      (err, stdout, stderr) => {
        if (err) {
          // node couldn't execute the command
          return;
        }
        console.log(`stdout: ${stdout}`);
        console.log(`stderr: ${stderr}`);
      }
    );
    fs.appendFile(
      `${projectPath}/src/models/index.ts`,
      `\nexport { default as ${
        json.info.title
      } } from './${json.info.title.toLowerCase()}.model'`,
      function(err) {
        if (err) throw err;
        console.log("Saved!");
      }
    );
    exec(
      `HYGEN_OVERWRITE=1 hygen controller new --params "${JSON.stringify(
        json
      ).replace(/"/g, '\\"')}"`,
      (err, stdout, stderr) => {
        if (err) {
          // node couldn't execute the command
          return;
        }
        console.log(`stdout: ${stdout}`);
        console.log(`stderr: ${stderr}`);
      }
    );
  } catch (e) {
    console.error(e);
    return res.sendStatus(500);
  }
  res.sendStatus(200);
});

module.exports = router;
