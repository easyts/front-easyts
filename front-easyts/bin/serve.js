#!/usr/bin/env node
const express = require("express");
const { resolve } = require("path");

const app = express();

app.use(express.json());
app.use("/hygen", require("../hygen.controller.js"));
app.use("/", express.static(resolve(__dirname, "..", "dist")));

console.log(resolve("."));

app.listen(3000, () => {
  console.log("Running at http://localhost:3000");
});
