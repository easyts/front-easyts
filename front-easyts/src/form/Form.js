import React, { useState } from "react";
import {
  MainContainer,
  FormContainer,
  StyledTitle,
  InputText,
  InputsForm,
  NewAttributeButton,
  NextButton,
  DisplayStepsContainer,
  ActiveStep,
  InactiveStep,
  StepLink,
  StyledLogo,
  SubmitButton,
  ReactTourLogo,
  StartTourLogo,
  TitleContainer,
  ButtonContainer,
  ModalContainer,
  ModalMessage,
  ModalLogo,
  ErrorSpan,
  InputWithErrorContainer
} from "./useStyle";
import Tour from "reactour";
import { createClass } from "../service/createClasse.api";
import axios from "axios";
import MinusLogo from "../assets/minus.svg";
import BusLogo from "../assets/bus.png";
import ConfirmLogo from "../assets/confirmation.png";
import { FormControl, InputLabel, Select } from "@material-ui/core";
import Modal from "react-modal";

function Form() {
  const [isTourOpen, setIsTourOpen] = useState(true);
  const [isShowingMore] = useState(false);
  const [attributesList, setAttributesList] = useState([
    { label: "", type: "", model: "" }
  ]);

  const [titleClass, setTitleClass] = useState("");

  const [displayStep2, setDisplayStep2] = useState(false);

  const inputLabel = React.useRef(null);
  const [labelWidth, setLabelWidth] = React.useState(0);
  const [modalIsOpen, setModalIsOpen] = React.useState(false);
  const [showTitleError, setShowTitleError] = React.useState(false);
  const [showModelError, setShowModelError] = useState(false);
  const [models, setModels] = useState("");

  React.useEffect(() => {
    setLabelWidth(inputLabel.current.offsetWidth);
    getModels();
  }, []);

  const addAttribute = e => {
    const newAttribute = [...attributesList];
    newAttribute.push({ label: "", type: "", model: "" });
    setAttributesList(newAttribute);
  };

  const removeAttribute = index => {
    if (attributesList.length > 1) {
      let newAttribute = [...attributesList];
      newAttribute.splice(index, 1);
      setAttributesList(newAttribute);
    }
  };

  const handleInputChange = (index, event) => {
    const values = [...attributesList];
    if (event.target.name === "label") {
      values[index].label = event.target.value;
    }
    if (event.target.name === "type") {
      values[index].type = event.target.value;
    }
    if (event.target.name === "model") {
      values[index].model = event.target.value;
    }

    setAttributesList(values);
  };

  function getModels() {
    const urlToGet = "http://localhost:8080/classes";
    try {
      axios.get(urlToGet, { crossdomain: true }).then(function(response) {
        console.log(response.data);
        setModels(response.data);
      });
    } catch (error) {
      console.error(error);
    }
  }

  async function callCreateClass() {
    const classInfos = {
      info: {
        title: titleClass
      },
      relation: {},
      attributes: {}
    };

    attributesList.forEach(attribute => {
      if (attribute.model) {
        classInfos.relation[attribute.model] = attribute.type;
        if (attribute.type === "BelongsTo") {
          classInfos.attributes[attribute.label] = attribute.model;
        } else if (attribute.type === "HasMany") {
          classInfos.attributes[attribute.label] = attribute.model + "[]";
        }
      } else {
        classInfos.attributes[attribute.label] = attribute.type;
      }
    });

    if (titleClass) {
      let created = createClass(classInfos);
      created.then(response => {
        if (response.status === 200) {
          axios
            .post(
              "http://localhost:8080/create",
              { model: classInfos.info.title },
              { crossdomain: true }
            )
            .then(response => {
              if (response.status === 200) {
                setModalIsOpen(true);
                console.log(200);
              }
            })
            .catch(error => {
              setShowModelError(true);
            });
        } else {
          return false;
        }
      });
      setShowTitleError(false);
    } else {
      setShowTitleError(true);
    }
  }

  const closeTour = () => {
    setIsTourOpen(false);
  };

  const openTour = () => {
    setIsTourOpen(true);
  };

  const tourConfig = [
    {
      selector: '[data-tut="steps"]',
      content: `There are 2 steps.`
    },
    {
      selector: '[data-tut="attributes"]',
      content: `Here you can choose you labels, your types and your relations`
    },
    {
      selector: '[data-tut="add-attributes"]',
      content: `With this button you can add as many attributes as you wish`
    },
    {
      selector: '[data-tut="remove-attributes"]',
      content: `With this button you can remove an attribute from the list`
    },
    {
      selector: '[data-tut="next"]',
      content: `This button allows you to access to the second step`
    },
    {
      selector: '[data-tut="title"]',
      content: `You can choose the name of your class with this input`,
      action: () => setDisplayStep2(true)
    },
    {
      selector: '[data-tut="create-class"]',
      content: `And finally you can create your class`
    }
  ];

  const modalStyle = {
    content: {
      top: "35%",
      left: "50%",
      right: "auto",
      bottom: "auto",
      width: "26%",
      height: "27%",
      zIndex: "1",
      transform: "translate(-50%, -50%)"
    }
  };

  const closeModal = () => {
    setModalIsOpen(false);
    setDisplayStep2(false);
  };

  return (
    <>
      <MainContainer openTour={isTourOpen} isShowingMore={isShowingMore}>
        {displayStep2 ? (
          <>
            <DisplayStepsContainer>
              <InactiveStep onClick={() => setDisplayStep2(false)}>
                1
              </InactiveStep>
              <StepLink />
              <ActiveStep onClick={() => setDisplayStep2(true)}>2</ActiveStep>
            </DisplayStepsContainer>
            <FormContainer>
              <StyledTitle>ADD A TITLE</StyledTitle>
              <InputWithErrorContainer>
                <InputText
                  variant="outlined"
                  values={titleClass}
                  onChange={e => setTitleClass(e.target.value)}
                  data-tut={"title"}
                ></InputText>
                {showTitleError ? (
                  <ErrorSpan>Please enter title</ErrorSpan>
                ) : null}
                {showModelError ? (
                  <ErrorSpan>Model already exist</ErrorSpan>
                ) : null}
              </InputWithErrorContainer>
              <InputsForm>
                <SubmitButton
                  onClick={() => callCreateClass()}
                  data-tut={"create-class"}
                >
                  Create class
                </SubmitButton>
              </InputsForm>
            </FormContainer>
          </>
        ) : (
          <>
            <DisplayStepsContainer data-tut={"steps"}>
              <ActiveStep onClick={() => setDisplayStep2(false)}>1</ActiveStep>
              <StepLink />
              <InactiveStep onClick={() => setDisplayStep2(true)}>
                2
              </InactiveStep>
            </DisplayStepsContainer>
            <FormContainer>
              <TitleContainer>
                <StyledTitle>CREATE YOUR CLASS</StyledTitle>
                <StartTourLogo onClick={() => openTour()}>
                  GUIDE
                  <ReactTourLogo src={BusLogo}></ReactTourLogo>
                </StartTourLogo>
              </TitleContainer>

              {attributesList ? (
                attributesList.map((attribute, index) => {
                  return (
                    <InputsForm key={index} data-tut={"attributes"}>
                      <InputText
                        label="Label"
                        variant="outlined"
                        name="label"
                        onChange={e => handleInputChange(index, e)}
                        value={attribute.label}
                      ></InputText>
                      <FormControl variant="outlined">
                        <InputLabel
                          ref={inputLabel}
                          htmlFor="outlined-age-native-simple"
                        >
                          Type
                        </InputLabel>
                        <Select
                          name="type"
                          id="type"
                          native
                          value={attribute.type}
                          onChange={e => handleInputChange(index, e)}
                          labelWidth={labelWidth}
                        >
                          <option value="" />
                          <option value="number">Number</option>
                          <option value="string">String</option>
                          <option value="enum">Enum</option>
                          <option value="any">Any</option>
                          <option value="Object">Object</option>
                          <option value="HasMany">HasMany</option>
                          <option value="BelongsTo">BelongsTo</option>
                          <option value="Number[]">Number [ ]</option>
                          <option value="String[]">String [ ]</option>
                          <option value="Object[]"> Object [ ]</option>
                        </Select>
                      </FormControl>
                      {attribute.type === "HasMany" ||
                      attribute.type === "BelongsTo" ? (
                        <FormControl variant="outlined">
                          <InputLabel
                            ref={inputLabel}
                            htmlFor="outlined-age-native-simple"
                          >
                            Model
                          </InputLabel>
                          <Select
                            name="model"
                            native
                            value={attribute.model}
                            onChange={e => handleInputChange(index, e)}
                            labelWidth={labelWidth}
                          >
                            <option value="" />
                            {models.map(model => {
                              return <option value={model}>{model}</option>;
                            })}
                          </Select>
                        </FormControl>
                      ) : null}
                      <StyledLogo
                        src={MinusLogo}
                        onClick={() => removeAttribute(index)}
                        data-tut={"remove-attributes"}
                      />
                    </InputsForm>
                  );
                })
              ) : (
                <InputsForm>
                  <InputText label="Label" variant="outlined"></InputText>
                  <InputText label="Type" variant="outlined"></InputText>
                </InputsForm>
              )}
              <NewAttributeButton
                variant="contained"
                onClick={() => addAttribute()}
                data-tut={"add-attributes"}
              >
                New attribute
              </NewAttributeButton>
              <ButtonContainer>
                <NextButton
                  onClick={() => setDisplayStep2(true)}
                  data-tut={"next"}
                >
                  Next
                </NextButton>
              </ButtonContainer>
            </FormContainer>
          </>
        )}
      </MainContainer>
      <Modal isOpen={modalIsOpen} style={modalStyle}>
        <ModalContainer>
          <ModalLogo src={ConfirmLogo}></ModalLogo>
          <ModalMessage>Your class is create</ModalMessage>
          <SubmitButton onClick={() => closeModal()}>Close</SubmitButton>
        </ModalContainer>
      </Modal>
      <Tour
        onRequestClose={() => closeTour()}
        steps={tourConfig}
        isOpen={isTourOpen}
        maskClassName="mask"
        className="helper"
        rounded={5}
      />
    </>
  );
}

export default Form;
