import styled from "styled-components";
import { Container, Card, TextField, Button, Select } from "@material-ui/core";

export const MainContainer = styled(Container)`
  margin-top: 5rem;
  z-index: 0;
`;

export const DisplayStepsContainer = styled.div`
  width: 10rem;
  margin: 0 auto 1rem auto;
  display: flex;
  flex-direction: row;
  justify-content: space-around;
`;

export const ActiveStep = styled.div`
  background: white;
  border-radius: 0.6rem;
  width: 1.8rem;
  text-align: center;

  :hover {
    cursor: pointer;
  }
`;

export const StepLink = styled.div`
  width: 7rem;
  height: 0.1rem;
  background: white;
  margin-top: 12px;
`;

export const InactiveStep = styled.div`
  background: #0071ca;
  border-radius: 0.5rem;
  width: 1.8rem;
  text-align: center;
  color: white;

  :hover {
    cursor: pointer;
  }
`;

export const FormContainer = styled(Card)`
    width: 40rem;
    margin: 5rem auto 0 auto;
    text-align center;
    border-radius: 0.8rem !important;
`;

export const TitleContainer = styled.div`
  display: flex;
  flex-direction: row;
  justify-content: flex-end;
`;

export const StyledTitle = styled.p`
  font-size: 36px;
  color: #666666;
`;

export const SubTitle = styled.p`
  font-size: 18px;
  color: #666666;
`;

export const InputsForm = styled.div`
  display: flex;
  flex-direction: row;
  margin: 1rem auto 0 auto;
  width: 30rem;
  justify-content: space-between;
`;

export const InputWithErrorContainer = styled.div`
  display: flex;
  flex-direction: column;
  width: 15rem;
  margin: auto;
`;

export const InputText = styled(TextField)``;

export const ErrorSpan = styled.span`
  color: red;
`;

export const InputSelect = styled(Select)``;

export const StyledLogo = styled.img`
  margin-top: 1rem;
  height: 1.5rem;
  width: auto;

  :hover {
    cursor: pointer;
  }
`;

export const NewAttributeButton = styled(Button)`
  background: #c4c4c4 !important;
  float: left;
  margin: 1rem 0 1rem 5rem !important;
  color: white !important;
  font-weight: bold !important;
`;

export const ButtonContainer = styled.div`
  width: 10rem;
  margin: 4rem 0 1rem auto;
`;

export const NextButton = styled(Button)`
  background: #0071ca !important;
  color: white !important;
  float: right;
  margin: 1rem !important;
  font-weight: bold !important;
`;

export const SubmitButton = styled(Button)`
  background: #0071ca !important;
  margin: 1rem auto 1rem auto !important;
  color: white !important;
  font-weight: bold !important;
`;

export const StartTourLogo = styled(Button)`
  background: #0071ca !important;
  float: right;
  margin: 0.6rem 0.6rem 0 4rem !important;
  color: white !important;
  font-size: 10px !important;
  font-weight: bold !important;
  height: 2rem;
`;

export const ReactTourLogo = styled.img`
  height: 1rem;
  width: auto;
  margin-left: 0.5rem;
`;

export const ModalContainer = styled.div`
  display: flex;
  flex-direction: column;
  align-items: center;
`;

export const ModalMessage = styled.p`
  margin: 0 auto 1rem auto;
  color: #003366;
`;

export const ModalLogo = styled.img`
  height: 4rem;
  width: auto;
  margin: 0 auto 2rem auto;
`;
