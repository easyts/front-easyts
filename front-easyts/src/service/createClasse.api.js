import axios from "axios";

export function createClass(params) {
  const urlToSend = "http://localhost:3000/hygen";
  return axios.post(urlToSend, params, { crossdomain: true });
}
