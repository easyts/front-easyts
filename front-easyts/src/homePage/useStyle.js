import styled from "styled-components";
import { Button } from "@material-ui/core";

export const TitleContainer = styled.div`
  display: flex;
  flex-direction: row;
  justify-content: space-evenly;
  margin-top: 15rem;
`;

export const StyledTitle = styled.div`
  color: white;
  width: 10rem;
  font-size: 5rem;
  font-family: Modak;
  margin-top: 2.5rem;
`;

export const StyledLogo = styled.img`
  width: 15rem;
  height: auto;
`;

export const StyledButton = styled(Button)`
  background: #2081c3 !important;
  width: 5rem !important;
  margin: auto !important;
  font-weight: bold !important;
  color: white !important;
`;

export const CenterButton = styled.div`
  width: 7rem;
  margin: auto;
`;
