import React from "react";
import {
  StyledTitle,
  TitleContainer,
  StyledLogo,
  StyledButton,
  CenterButton
} from "../homePage/useStyle";
import { Link } from "react-router-dom";
import easytsLogo from "../assets/logo ts.png";
function HomePage() {
  return (
    <>
      <TitleContainer>
        <StyledTitle>EasyTs</StyledTitle>
        <StyledLogo src={easytsLogo} />
      </TitleContainer>
      <CenterButton>
        <Link to="/easyts">
          <StyledButton>Start</StyledButton>
        </Link>
      </CenterButton>
    </>
  );
}

export default HomePage;
