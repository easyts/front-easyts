import React from "react";
import HomePage from "./homePage/homePage";
import Form from "./form/Form";
import { BrowserRouter as Router, Route, Switch } from "react-router-dom";
import "./App.css";

const App = () => {
  return (
    <Router>
      <Switch>
        <Route exact path="/">
          <HomePage />
        </Route>
        <Route path="/easyts">
          <Form />
        </Route>
      </Switch>
    </Router>
  );
};

export default App;
